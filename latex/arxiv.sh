#!/bin/bash
mkdir arxiv || { echo "error, you need to (rmdir arxiv)"; exit 1; }

cp article.{tex,cfg} arxiv/
cp examples.tex arxiv/

for f in article.bib amal.bib
do
    cp $f arxiv/
done

for f in boolean copytofile fixtheorems lib lrlib \
         mathpartir mttex mttex_linear reversion unlist
do
    cp $f.sty arxiv/
done

## only necessary for the ACM version
# for f in acmart.cls
# do
#     cp $f arxiv/
# done

pdflatex article.tex
bibtex article.aux
cp article.bbl arxiv/

echo "pdflatex article.tex" >> arxiv/build.sh
for i in 1 2 3
do
    echo "pdflatex article.tex" >> arxiv/build.sh
done

zip -r arxiv arxiv
