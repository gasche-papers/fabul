\RequirePackage{boolean}
\newcommand{\Beamer}{\True}

\documentclass[svgnames,notheorems]{beamer}

%\usetheme{Madrid} % My favorite!
\usetheme{Boadilla} % Pretty neat, soft color.
%\usetheme{default}
%\usetheme{Warsaw}
%\usetheme{Bergen} % This template has nagivation on the left
% \usetheme{Frankfurt} % Similar to the default
%with an extra region at the top.
%\usecolortheme{seahorse} % Simple and clean template
%\usetheme{Darmstadt} % not so good
% Uncomment the following line if you want %
% page numbers and using Warsaw theme%
% \setbeamertemplate{footline}[page number]
%\setbeamercovered{transparent}
\setbeamercovered{invisible}
% To remove the navigation symbols from
% the bottom of slides%
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{\hfill\insertframenumber\hfill\vspace{3mm}}

% to avoid confusion with \ob{} color (blue), use orange-like titles
% \setbeamercolor*{titlelike}{fg=Orange!80!Purple}

\let\Title\title
% beamer defined \title, we want to keep it as \Title as mttex will redefine it

\usepackage[omit,todo]{mttex}
\usepackage{lib}
\usepackage{beamer_lib}

\usepackage{tikz}
\usepackage[all]{xy}

\Title{Full abstraction for multi-language systems\\ ML plus linear types}
\author{\textbf{Gabriel Scherer}, Max New, Nicholas Rioux, Amal Ahmed}

\institute{INRIA Saclay, France\\
  Northeastern University, Boston}
\date{\today}

% \usepackage{tcolorbox}

% \tcbset{boxsep=-1mm,boxrule=0.3mm,colback=white}
% \newtcbox{\noninvertiblerule}{colframe=red!75!black}
% \newtcbox{\invertiblerule}{colframe=green!75!black}

\begin{document}
%
\newcommand{\showface}[1]{\includegraphics[height=2.3cm]{#1}}
\begin{frame}
  \titlepage

\begin{center}
  \showface{faces/Gabriel.jpg}
  \showface{faces/Max.jpg}
  \showface{faces/Nicholas.jpg}
  \showface{faces/Amal.jpg}
\end{center}
\end{frame}

\begin{frame}
  \tableofcontents
\end{frame}

\section{Full abstraction for multi-language systems: introduction}
\frame{\sectionpage}

\begin{frame}
  \frametitle{Multi-language systems}

  Languages of today tend to evolve into behemoths by piling features
  up: C++, Scala, GHC Haskell, OCaml...

  \vfill

  Multi-language systems: several languages working together to cover
  the feature space. {\color{gray}{(simpler?)}}

  \vfill

  Multi-language system \emph{design} may include designing new
  languages for interoperation.

  \vfill

  Full abstraction to understand graceful language interoperability.
\end{frame}

\begin{frame}
  \frametitle{Multi-language stories}

\begin{tikzpicture}
  \begin{scope}[blend group = soft light]
    \fill[red!30!white]   (  0:1.4) circle (1);
    \fill[blue!30!white]  (180:1.2) circle (1.8);
  \end{scope}
  \node at (  0:1.4)     {$\begin{array}{c}\text{Expert}\\
                           \text{language}\end{array}$};
  \node at (180:1.4)   {$\begin{array}{c}\text{General-purpose}\\
                         \text{language}\end{array}$};
\end{tikzpicture}\qquad\qquad
\begin{tikzpicture}
  \begin{scope}
    \fill[red!30!white]   (0:0) circle (2);
    \fill[blue!30!white]  (0:0.7) circle (1);
  \end{scope}
  \node at (  0:0.7)     {$\begin{array}{c}\text{Teachable}\\
                           \text{sublanguage}\end{array}$};
  \node at (180:1.1)   {$\begin{array}{c}\text{Wild}\\
                         \text{language}\end{array}$};
\end{tikzpicture}

Graceful interoperation?
\hfill Abstraction leaks?

\vfill

(Several expert languages: not (yet?) in this work)
\end{frame}

\begin{frame}
  \frametitle{A question worth asking}

  What does it \emph{mean} for two languages to ``interact well together''?

  \vfill\pause

  \begin{itemize}
  \item no segfaults?
    \pause
  \item the type systems are not broken?

    (correspondence between types on both sides)
    \pause
  \item more?
  \end{itemize}

\pause

  \begin{mathpar}
    \llbracket \_ \rrbracket : Types(L1) \to Types(L2)

    \infer
    {\Gamma \vdash_{L1} e : \tau}
    {\Gamma \vdash_{L2} L1(e) : \llbracket \tau \rrbracket}

    + \text{type soundness of the combined system}
  \end{mathpar}
\end{frame}

\begin{frame}
  \frametitle{Full abstraction}

  $\sembrace \wild : S \longrightarrow T$ fully abstract:
  \begin{mathpar}
    a \mathrel{\ctxeqvsym} b
    \implies
    \sembrace a \mathrel{\ctxeqvsym} \sembrace b
  \end{mathpar}

  \vfill

  Full abstraction preserves (equational) reasoning.
\end{frame}

\begin{frame}
  \frametitle{Full abstraction for multi-language systems}

\begin{tikzpicture}
  \begin{scope}[blend group = soft light]
    \fill[red!30!white]   (  0:1.4) circle (1);
    \fill[blue!30!white]  (180:1.2) circle (1.8);
  \end{scope}
  \node at (  0:1.4)     {$\begin{array}{c}\text{Expert}\\
                           \text{language}\end{array}$};
  \node at (180:1.4)   {$\begin{array}{c}\text{General-purpose}\\
                         \text{language}\end{array}$};
\end{tikzpicture}\qquad\qquad
\begin{tikzpicture}
  \begin{scope}
    \fill[red!30!white]   (0:0) circle (2);
    \fill[blue!30!white]  (0:0.7) circle (1);
  \end{scope}
  \node at (  0:0.7)     {$\begin{array}{c}\text{Teachable}\\
                           \text{sublanguage}\end{array}$};
  \node at (180:1.1)   {$\begin{array}{c}\text{Wild}\\
                         \text{language}\end{array}$};
\end{tikzpicture}

Graceful interoperation:
$\textcolor{blue}{G}
\stackrel{f.a.}{\longrightarrow}
(\textcolor{blue}{G} + \textcolor{red}{E})$

\hfill No abstraction leaks:
$\textcolor{blue}{T}
\stackrel{f.a.}{\longrightarrow}
\textcolor{red}{W}$
\end{frame}

\begin{frame}
  \frametitle{Which languages?}

  ML sweet spot hard to beat,

  but ML programmers yearn for language extensions.

  \vfill

  ML plus:
  \begin{itemize}
  \item low-level memory, resource tracking, ownership
  \item effect system
  \item theorem proving
  \item \dots
  \end{itemize}

  \vfill

  In this talk: a first ongoing experiment on \emph{ML} plus \emph{linear types}.
\end{frame}

\begin{frame}
  \frametitle{Our case study}

  \textcolor{blue}{U} (Unrestricted): general-purpose ML language

  \textcolor{red}{L} (Linear): expert linear language.

  \vfill

  \[
    \textcolor{blue}{U}
    \stackrel{f.a.}{\longrightarrow}
    (\textcolor{blue}{U} + \textcolor{red}{L})
  \]

  \vfill

  Proof: by translating $\textcolor{red}{L}$ back into
  $\textcolor{blue}{U}$ in an inefficient but correct way.

  \vfill\pause

  Note: extending $\textcolor{blue}{U}$ preserves this result.

  \vfill\pause

  Note: $\textcolor{red}{L} \longrightarrow
  (\textcolor{blue}{U} + \textcolor{red}{L})$ not meant to be fully abstract.

  (Not robust to extensions of $\textcolor{blue}{U}$)
\end{frame}

\section{Case study: Unrestricted and Linear}
\frame{\sectionpage}

\begin{frame}
  \frametitle{Unrestricted language: syntax}
  \begin{displaymath}
    \begin{array}{lcrl}
      \mbox{\textit{Types}} &
      \uty
      & \bnfdef
      & \ualpha
        \bnfalt
        \upairty \utyone \utytwo
        \bnfalt
        \uunitty
        \bnfalt
        \ufunty \utyone \utytwo
        \bnfalt
      \\ & &
      & \usumty \utyone \utytwo
        \bnfalt
        \umuty \ualpha \uty
        \bnfalt
        \uforallty \ualpha \uty
        % \bnfalt
        % \uexistty \ualpha \uty
        % \bnfalt
        % \urefty \uty
      \\[6pt]

      \mbox{\textit{Expressions}} &
      \ue & \bnfdef
      & \ux
        \bnfalt \\ & &
      & \upaire \ueone \uetwo
        \bnfalt
        \uprje 1 \ue
        \bnfalt
        \uprje 2 \ue
        \bnfalt
      \\ & &
      & \uunite
        \bnfalt
        \uletunite \ueone \uetwo
        \bnfalt
      \\ & &
      & \ufune \ux \uty \ue
        \bnfalt
        \uappe \ueone \uetwo
        \bnfalt
      \\ & &
      & \usume 1 \ue \bnfalt \usume 2 \ue
        \bnfalt
        \ucasee \uepr \uxone \ueone \uxtwo \uetwo
        \bnfalt
      \\ & &
      & \ufolde {\umuty \ualpha \uty} \ue
        \bnfalt
        \uunfolde \ue
        \bnfalt
      \\ & &
      & \uabstre \ualpha \ue
        \bnfalt
        \uinste \ue \uty
      % \\ & &
      % & \upacke \uty \ue {\uexistty \ualpha \utypr}
      %   \bnfalt
      %   \uunpacke \ualpha \ux \ue \uepr
      % \\ & &
      % & \uloc
      %   \bnfalt
      %   \urefe \ue
      %   \bnfalt
      %   \uderefe \ue
      %   \bnfalt
      %   \usete \ue \uepr
      % \\ & &
      % & \upolyfunty \ualpha \uty \utypr
      %   \bnfalt
      %   \upolyfune \ualpha \ux \uty \ue
      %   \bnfalt
      %   \upappe \ue \uty \uepr
      \\[6pt]

      \mbox{\textit{Typing contexts}} & \uGamma, \uDelta & \bnfdef
      & \uempty
        \bnfalt
        \uGamma, \var \ux \uty
        \bnfalt
        \uGamma, \ualpha
      \\[6pt]
    \end{array}
  \end{displaymath}
\end{frame}

\newcommand{\termpart}[1]{\Alt<2->{#1}{}}
\newcommand{\simplejudg}[3]{#1 \vdash \termpart{#2 :} #3}
\newcommand{\ljudgsimple}[5]{\simplejudg{#2}{#4}{#5}}
\newcommand{\ljudgsimplenoconf}[3]{\simplejudg{#1}{#2}{#3}}

\begin{frame}
  \frametitle{Linear types: introduction}

  Resource tracking, unique ownership.

  \begin{mathpar}
    \lty

    \lbangty \lty

    \lGamma

    \lbangty \lGamma
    \\
    \ljudgnoconf {\lGamma} \le \lty
  \end{mathpar}

  We own $\le$ at type $\lty$ (duplicable or not),
  $\le$ owns the resources in $\lGamma$.

  \begin{mathpar}
    \begin{array}{l@{~}c@{~}r@{~}ll}
      & \lty
      & \bnfdef
      & \lopairty \ltyone \ltytwo
        \bnfalt
        \lunitty
        \bnfalt
        \lofunty \ltyone \ltytwo
        \bnfalt
      \\ & &
      & \losumty \ltyone \ltytwo
        \bnfalt
        \lmuty \lalpha \lty \bnfalt
        \lalpha
        \bnfalt
      \\ & &
      & \lbangty \lty
        \bnfalt
      \\ & &
      & \lboxty \lb \lty
    \end{array}
  \end{mathpar}
\end{frame}

\begin{frame}
  \frametitle{Linear types: base}

  A \emph{simple} but \emph{useful} language with linear types.
  \begin{mathpar}
    \inferrule
    { }
    {\ljudgnoconf {\lbangty \lGamma, \var \lx \lty} \lx \lty}

    \inferrule
    { }
    {\ljudgnoconf {\lbangty \lGamma} \lunite \lunitty}

    \inferrule
    {\ljudgnoconf \lGamma \le \lunitty \+
     \ljudgnoconf \lDelta \lepr \lty}
    {\ljudgnoconf
      {\ctxjoin \lGamma \lDelta}
      {\lletunite \le \lepr}
      \lty}
\\
    \inferrule
    {\ljudgnoconf \lGammaone \leone \ltyone \+
     \ljudgnoconf \lGammatwo \letwo \ltytwo}
    {\ljudgnoconf
      {\ctxjoin \lGammaone \lGammatwo}
      {\lpaire \leone \letwo}
      {\lopairty \ltyone \ltytwo}}
    \quad
    \inferrule
    {\ljudgnoconf \lGamma \le {\lopairty \ltyone \ltytwo} \quad
      \ljudgnoconf {\lDelta, \var \lxone \ltyone, \var \lxtwo \ltytwo}
        \lepr \lty}
    {\ljudgnoconf
      {\ctxjoin \lGamma \lDelta}
      {\lletpaire \lxone \lxtwo \le \lepr}
      \lty}
\\
    \inferrule
    {\ljudgnoconf {\lGamma, \var \lx \lty} \le \ltypr}
    {\ljudgnoconf \lGamma {\lfune \lx \lty \le} {\lofunty \lty \ltypr}}

    \inferrule
    {\ljudgnoconf \lGamma \le {\lofunty{\ltypr}{\lty}} \+
     \ljudgnoconf \lDelta \lepr \ltypr}
    {\ljudgnoconf
      {\ctxjoin \lGamma \lDelta}
      {\lappe \le \lepr}
      \lty}
\\
    \inferrule
    {\ljudgnoconf \lGamma \le \ltyi}
    {\ljudgnoconf \lGamma
      {\lsume \ix \le} {\losumty \ltyone \ltytwo}}

    \inferrule
    {\ljudgnoconf \lGamma \le {\losumty \ltyone \ltytwo}
     \+
     (\ljudgnoconf {\lDelta,\lxi:\ltyi} \lei \lty)_{\lfont i \in \{\lfont 1, \lfont 2\}}}
    {\ljudgnoconf
      {\ctxjoin \lGamma \lDelta}
      {\lcasee \le \lxone \leone \lxtwo \letwo}
      \lty}
\\
    \inferrule
    {\ljudgnoconf {\lbangty \lGamma} \le \lty}
    {\ljudgnoconf
      {\lbangty \lGamma}
      {\lsrcsharee \le}
      {\lbangty \lty}}

    \inferrule
    {\ljudgnoconf \lGamma \le {\lbangty \lty}}
    {\ljudgnoconf \lGamma {\lcopye {\const\lty} \le} \lty}

    \bidirliniso
      {\lmuty \lalpha \lty}
      {\lunfoldedty \lalpha \lty}
      {\lunfolde {}}
      {\lfolde {\lmuty \lalpha \lty} {}}
  \end{mathpar}
\end{frame}

  \newcommand{\lPath}{\llumpedty{\ufont{Path}}}
  \newcommand{\lString}{\llumpedty{\ufont{String}}}
  \newcommand{\lHandle}{\lfont{Handle}}
  \newcommand{\lEmptyHandle}{\lfont{EmptyHandle}}

\begin{frame}
  \frametitle{Applications}

  Protocol with resource handling requirements.

  \vspace{1em}

  ``This file descriptor must be closed''

  \vfill

  \begin{displaymath}
  \begin{array}{lll}
    \lfont{open} & :
    & \lbangty{(\lofunty \lPath \lHandle)}
    \\
    \lfont{line} & :
    & \lbangty
        {(\lofunty \lHandle
          {(\losumty \lHandle {(\lopairty \lString \lHandle)})})}
    \\
    \lfont{close} & :
    & \lbangty{(\lofunty \lHandle \lunitty)}
  \end{array}
  \end{displaymath}

  ~

  \textcolor{gray}{(details about the boundaries come later)}

  \vfill

  Typestate.
\end{frame}

\begin{frame}[fragile]
 \textcolor{gray}{(details about the boundaries come later)}

\vfill

\begin{displaymath}
  \begin{array}{lll}
    \lfont{open} & :
    & \lbangty{(\lofunty \lPath \lHandle)}
    \\
    \lfont{line} & :
    & \lbangty
        {(\lofunty \lHandle
          {(\losumty \lHandle {(\lopairty \lString \lHandle)})})}
    \\
    \lfont{close} & :
    & \lbangty{(\lofunty \lHandle \lunitty)}
  \end{array}
\end{displaymath}

\begin{lstlisting}
/!let concat_lines path : String = UL(/*
  loop (open LU(/!path!/)) LU(/!Nil!/)
  where rec loop handle LU(/!acc : List String!/) =
    match line handle with
    | EOF handle ->
      close handle; LU(/!rev_concat "\n" acc!/)
    | Next line handle ->
      loop handle LU(/!Cons UL(/*line*/) acc!/)*/)!/
\end{lstlisting}

\vfill

\textcolor{gray}{(U values are passed back and forth, never inspected)}
\end{frame}

\begin{frame}
  \frametitle{Linear types: linear locations}

  $\lboxty \lone \lty$: full cell

  \vspace{1em}

  $\lboxty \lzero {}$: empty cell

  \vfill

  \begin{mathpar}
  \bidirliniso
    {\lunitty}
    {\lboxty \lzero {}}
    {\lnewe {}}
    {\lfreee {}}

  \bidirliniso
    {\lboxedty \lty}
    {\lunboxedty \lty}
    {\lunboxe {}}
    {\lboxe {}}
  \end{mathpar}

  \vfill

  Applications: in-place reuse of memory cells.
\end{frame}

\begin{frame}[fragile]
\frametitle{List reversal}

\begin{lstlisting}
/*
  type LList a = $\lfontsym\mu$t. 1 $\lfontsym\oplus$ Box 1 (a $\lfontsym\otimes$ t)

  val reverse : LList a $\lfontsym\multimap$ LList a
  let reverse list = loop (inl ()) list
    where rec loop tail = function
    | inl () $\lfontsym\to$ tail
    | inr cell $\lfontsym\to$
      let (l, (x, xs)) = unbox cell in
      let cell = box (l, (x, tail)) in
      loop (inr cell) xs
*/
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{List reversal (sweet)}

\begin{lstlisting}
/*
  type LList a = $\lfontsym\mu$t. 1 $\lfontsym\oplus$ Box 1 (a $\lfontsym\otimes$ t)
  pattern Nil = inl ()
  pattern Cons l x xs = inr (box (l, (x, xs)))

  val reverse : LList a $\lfontsym\multimap$ LList a
  let reverse list = loop Nil list
    where rec loop tail = function
    | Nil $\lfontsym\to$ tail
    | Cons l x xs $\lfontsym\to$ loop (Cons l x tail) xs$\pause$
*//!
  type List a = $\ufontsym\mu$t. 1 + (a $\ufontsym\times$ t)
  let reverse list = UL(/*share (reverse (copy (LU(/!list!/))))*/)
!/
\end{lstlisting}

\textcolor{gray}{(U values are created from the L side from a compatible type)}
\end{frame}

\begin{frame}[fragile]
\begin{lstlisting}
/*
let partition p li = partition_aux p (Nil, Nil) li
partition_aux p (yes, no) = function
| Nil -> (yes, no)
| Cons l x xs ->
  let (yes, no) =
    if copy p x then (Cons l x yes, no) else (yes, Cons l x no)
  in partition_aux p (yes, no) xs

let lin_quicksort li = quicksort_aux li Nil
let quicksort_aux li acc = match li with
| Nil -> acc
| Cons l head li ->
  let p = share (fun x -> x < head) in
  let (below, above) = partition p li in
  quicksort_aux below (Cons l head (quicksort_aux above acc))*/

/!quicksort li UL(/*li*/) = UL(/*share (lin_quicksort (copy li))*/)!/
\end{lstlisting}  
\end{frame}

\renewcommand{\defeq}{\stackrel{\text{def}}{=}}
\begin{frame}[fragile]
  \frametitle{Interaction: lump}

  \begin{mathpar}
    \begin{array}{l@{~}c@{~}r@{~}ll}
      \mbox{\textit{Types}}
      & \uty \bnfalt \lty & &
      \\[2pt]
      & \uty & &
      \\
      & \lty & \bnfadd & \dots \bnfalt \llumpty\uty
      \\[6pt]
    \end{array}

    \begin{array}{l@{~}c@{~}r@{~}ll}
      \mbox{\textit{Expressions}}
      & \ue \bnfalt \le & &
      \\[2pt]
      & \ue & \bnfadd & \dots \bnfalt \ULenoconf \le
      \\
      & \le & \bnfadd & \dots \bnfalt \LUe \ue
      \\[6pt]
    \end{array}

    \begin{array}{l@{~}c@{~}r@{~}ll}
      \mbox{\textit{Contexts}}
      & \ulGamma
      & \bnfdef
      & \ulemptyGamma
        \bnfalt
        \ulGamma, \var \ux \uty
        \bnfalt
        \ulGamma, \ualpha
        \bnfalt
        \ulGamma, \var \lx \lty
    \end{array}
  \end{mathpar}

  \begin{mathpar}
  \infer
  {\ulujudg \ulbGamma \ue \uty}
  {\ulljudgnoconf \ulbGamma {\LUe \ue}
    {\llumpedty \uty}}

  \infer
  {\ulljudgnoconf \ulbGamma \le {\llumpedty \uty}}
  {\ulujudg \ulbGamma {\ULenoconf \le} \uty}
  \end{mathpar}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Interaction: compatibility}

  \vspace{6pt}
  Compatibility relation
  \quad
  \fbox{\small$\ulwf \uty \ulcompat \lty$}

  \begin{mathpar}
    \infer
    { }
    {\ulwf \uunitty \ulcompatbang \lunitty}

    \infer
    {\ulwf \utyone \ulcompatbang \ltyone\\
     \ulwf \utytwo \ulcompatbang \ltytwo}
    {\ulwf
      \upairty \utyone \utytwo
      \ulcompatbangpar{\lopairty \ltyone \ltytwo}}

    \infer
    {\ulwf \utyone \ulcompatbang \ltyone\\
     \ulwf \utytwo \ulcompatbang \ltytwo}
    {\ulwf
      \usumty \utyone \utytwo
      \ulcompatbangpar{\losumty \ltyone \ltytwo}}

    \infer
    {\ulwf \uty \ulcompatbang \lty\\
     \ulwf \utypr \ulcompatbang \ltypr}
    {\ulwf
      \ufunty \uty \utypr
      \ulcompatbangpar{\lofunty {\lbangty \lty} {\lbangty \ltypr}}}


    \infer
    { }
    {\ulwf \uty \ulcompat {\llumpedty \uty}}

    \infer
    {\ulwf \uty \ulcompatbang \lty}
    {\ulwf \uty \ulcompatbang {\lbangty \lty}}

    \infer
    {\ulwf \uty \ulcompatbang \lty}
    {\ulwf \uty \ulcompatbangpar {\lboxedty \lty}}
  \end{mathpar}

  \vspace{6pt} Interaction primitives and derived constructs:
  \begin{mathpar}
  \bidirliniso
    {\llumpedty \uty}
    {\lty}
    {\unlump \lty {}}
    {\lump \lty {}}
  \quad
  \text{when}
  \quad
  \ulwf \uty \ulcompat \lty
  \qquad
  \begin{array}{l}
    \LUmpe \lty \ue \;\defeq\; \unlump \lty {\LUe \ue}
    \\
    \UmpLenoconf \lty \le \;\defeq\; \ULenoconf {\lump \lty \le}
  \end{array}
  \end{mathpar}
\end{frame}

\begin{frame}
  \frametitle{Full abstraction}

  \begin{theorem}
    The embedding of $\usymbol$ into $\ulsymbol$ is fully abstract.
  \end{theorem}

  Proof: by pure interpretation of the linear language into ML.\pause

\begin{mathpar}
  \begin{array}{lll}
    \lift {\lbangty \lty}
    & \defeq
    & \lift \lty
    \\ \lift {\lboxty \lzero \lty}
    & \defeq
    & \uunitty
    \\ \lift {\lboxty \lone \lty}
    & \defeq
    & \upairty \uunitty {\lift \lty}
    \\ \lift {\lopairty \ltyone \ltytwo}
    & \defeq
    & \upairty {\lift \ltyone} {\lift \ltytwo}
  \end{array}
\end{mathpar}

{\color{gray}{(Cogent)}}
\end{frame}

\begin{frame}
  \frametitle{Remark on parametricity}

  (from Max New)

  \vfill

  \begin{mathpar}
    \uinste
    {(\uabstre \ualpha
      {\ufune \ux \ualpha
        {\UmpLenoconf \ualpha
          {\LUmpe \ualpha \ux}}})}
    \uty

    \uredexstep?

    {\ufune \ux \uty
        {\UmpLenoconf \uty
          {\LUmpe \uty \ux}}}
  \end{mathpar}

  Not well-typed!

  \vfill

  \begin{mathpar}
    \uinste
    {(\uabstre \ualpha
      {\ufune \ux \ualpha
        {\UmpLenoconf {\llumpedty \ualpha}
          {\LUmpe {\llumpedty \ualpha} \ux}}})}
    \uty

    \uredexstep

    {\ufune \ux \uty
        {\UmpLenoconf {\llumpedty \uty}
          {\LUmpe {\llumpedty \uty} \ux}}}
  \end{mathpar}

  \vfill

  Logical relation (Max New, Nicholas Rioux)
\end{frame}

\begin{frame}
  \vfill

  \begin{center}
    Questions?
  \end{center}
  \vfill
\end{frame}

\section{How fully abstract can we go?}
\frame{\sectionpage}

\begin{frame}
  I used to think of Full Abstraction as an \emph{ideal} property that
  would never be reached in practice.

  \vfill

  I changed my mind. The statement can be \emph{weakened} to fit many
  situations, and remains a useful specification.

  \vfill

  I will now present some (abstract) examples of this approach.
\end{frame}

\begin{frame}
  \frametitle{Weak Trick 1: restrict the interaction types}

  The no-interaction multi-language: always fully abstract!

  \vfill

  Types restrict interaction: ``only integers'', ``only ground
  types''.

  \vfill

  Extend the scope of safe interaction by adding more types.

  \emph{Design tool}.

  \vfill

  Idea: the idealist will still have a useful system.
\end{frame}

\begin{frame}
  \frametitle{Weak Trick 2: weaken the source equivalence}

  Full abstraction is relative to the source equivalence.

  \vfill

  Contextual equivalence makes a closed-world assumption.

  Good, sometimes too strong.

  \vfill

  Safe impure language: forbid reordering of calls.

  \vfill

  Safe impure language: add impure counters for user reasoning.

  \vfill

  Or use types with weaker equivalence principles: \emph{linking
    types}

  (Daniel Patterson, Amal Ahmed)

  \vfill

  Idea: full abstraction forces you to \emph{specify} the right thing.
\end{frame}

\begin{frame}
  \frametitle{Questions}

  Compare different ways to specify a weaker equivalence for full abstraction?
  \begin{itemize}
  \item through explicit term equations?
  \item through types?
  \item by adding phantom features?
  \end{itemize}

  \vfill

  Does our multi-language design scale to more than two languages?

  \textcolor{gray}{(Yes, I think)}

  \vfill

  Are boundaries multi-language designs also convenience boundaries?

  \textcolor{gray}{(good or bad?)}

  \vfill

  Your questions.

  \vfill

  \begin{center}
    Thanks!
  \end{center}
\end{frame}

\end{document}
