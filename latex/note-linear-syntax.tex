\pdfoutput=1
\RequirePackage{boolean}
\RequirePackage{reversion}

\newcommand{\Beamer}{\False}
\newcommand{\Print}{\False}


\documentclass{article}

\usepackage[nohyperref,notitlecaps]{mttex}

\usepackage{lib}
\usepackage{lrlib}

\title{On a programming syntax for linear logic\\
(UPenn seminar homework assignment)}
\author{Gabriel Scherer}

\newcommand{\lder}{\vdash_{\textsc{l}}}

\begin{document}

\maketitle

Linear logic is a sub-structural logic whose propositions (types) can
be thought of as ``resources''. A derivation of a linear judgment of
the form $\lGamma \lder \lty$ ($\textsc{L}$ for {\bf L}inear) explains
how to ``produce'' \emph{exactly one} proof of $\lty$ by ``consuming''
each assumption of $\lGamma$ exactly once -- they cannot be used
several times, and they cannot be discarded without use. Because of
this resource interpretation, it is natural (for programmers) to think
about linear logic as a programming language: one can think of a proof
of a linear proposition as a ``value'' that gets produced and
consumed, and of a derivation as organizing a flow of these values
from its assumptions to its conclusion(s).

The present note supports a talk to be given at UPenn on June 2nd,
2017. In this note, I will not give a comprehension presentation of
linear logic, but give enough information for people unfamiliar with
it to follow, and explain a minor syntactic change to the common
presentation of linear logic that makes it easier to use as
a programming language -- I will use this slightly modified
presentation in my talk. The modification is not an particularly
original idea, for example it occurs at the beginning an old note of
David Walker and Kevin Watkins.

You may have already encountered this presentation, in which case you
can stop reading the note at the end of this paragraph. The idea is to
allow variables of duplicable type $\lbangty \lty$ to be used several
times (instead of being renamed by contraction) by defining
a context-joining operation $\ctxjoin \lGamma \lGammapr$ that imposes
disjoint union of linear variables, but allows non-disjoint union of
variables of duplicable type.

Here are the propositions (types) of a simplified fragment of linear
logic that we will discuss:
\begin{mathpar}
  \begin{array}{l@{~}c@{~}r@{~}ll}
    &
      \lty
      & \quad \bnfdef \quad
      & \lopairty \ltyone \ltytwo
        \bnfalt
        \lunitty
        \bnfalt
        \lofunty \ltyone \ltytwo
        \bnfalt
        \losumty \ltyone \ltytwo
        \bnfalt
        \lbangty \lty
  \end{array}
\end{mathpar}

$\lopairty \ltyone \ltytwo$ is a linear conjunction (linear pair):
a proof of this type produces exactly one $\ltyone$ and exactly one
$\ltytwo$. $\lunitty$ is the unit type that can be produced from
nothing. $\lofunty \ltyone \ltytwo$ is a linear assumption, which one
can think of as a function type that (1) uses its input exactly once
and (2) will be \emph{called} exactly once. $\losumty \ltyone \ltytwo$
is a linear disjunction (linear sum), that contains either exactly
$\ltyone$ or exactly one $\ltytwo$. Finally, $(\lbangty)$ is
a modality that makes propositions (types) duplicable / non-linear: if
$\lty$ is a type of resource that must be used exactly once, then
$\lbangty \lty$ represents a type of resources that can be used
several times, or discarded. Having $(\lbangty)$ means that we can
express any proof of ``normal'' intuitionistic logic, where
assumptions can be used several times; an intuitionistic proof of
$\ltyone, \ltytwo \lder \lty$ can be turned into a linear proof of
$\lbangty \ltyone, \lbangty \ltytwo \lder \lbangty \lty$.

\section{The common (two-sided, natural deduction) presentation of (intuitionistic, propositional) linear logic}

There are many ways to present the inference rules of linear logic,
but a common one is the following:

\begin{mathpar}
\newcommand{\ljudgnoterm}[3]{#1 \lder #3}
\renewcommand{\var}[2]{#2}
    \inferrule[exchange]
    {\lGammaone, \ltytwo, \ltyone, \lGammatwo \lder \ltypr}
    {\lGammaone, \ltyone, \ltytwo, \lGammatwo \lder \ltypr}

    \inferrule[contraction]
    {\lGamma, \lbangty \lty, \lbangty \lty \lder \ltypr}
    {\lGamma, \lbangty \lty \lder \ltypr}

    \inferrule[weakening]
    {\lGamma \lder \ltypr}
    {\lGamma, \lbangty \lty \lder \ltypr}
\\
    \inferrule[axiom]
    { }
    {\ljudgnoterm {\var \lx \lty} \lx \lty}

    \inferrule
    {\ljudgnoterm \lGammaone \leone \ltyone \+
     \ljudgnoterm \lGammatwo \letwo \ltytwo}
    {\ljudgnoterm
      {\lGammaone, \lGammatwo}
      {\lpaire \leone \letwo}
      {\lopairty \ltyone \ltytwo}}

    \inferrule
    {\begin{array}{c}
       \ljudgnoterm \lGamma \le {\lopairty \ltyone \ltytwo} \\
       \ljudgnoterm {\lGammapr, \var \lxone \ltyone, \var \lxtwo \ltytwo}
         \lepr \lty
     \end{array}}
    {\ljudgnoterm
      {\lGamma, \lGammapr}
      {\lletpaire \lxone \lxtwo \le \lepr}
      \lty}
\\
    \inferrule
    { }
    {\ljudgnoterm {} \lunite \lunitty}

    \inferrule
    {\ljudgnoterm \lGamma \le \lunitty \+
     \ljudgnoterm \lGammapr \lepr \lty}
    {\ljudgnoterm
      {\lGamma, \lGammapr}
      {\lletunite \le \lepr}
      \lty}

    \inferrule
    {\ljudgnoterm {\lGamma, \var \lx \lty} \le \ltypr}
    {\ljudgnoterm \lGamma {\lfune \lx \lty \le} {\lofunty \lty \ltypr}}

    \inferrule
    {\ljudgnoterm \lGamma \le {\lofunty{\ltypr}{\lty}} \+
     \ljudgnoterm \lGammapr \lepr \ltypr}
    {\ljudgnoterm
      {\lGamma, \lGammapr}
      {\lappe \le \lepr}
      \lty}

    \inferrule
    {\ljudgnoterm \lGamma \le \ltyi}
    {\ljudgnoterm \lGamma
      {\lsume \ix \le} {\losumty \ltyone \ltytwo}}

    \inferrule
    {\begin{array}{c}\\
       \ljudgnoterm \lGamma \le {\losumty \ltyone \ltytwo}
     \end{array} 
     \+
     \begin{array}{c}
     \ljudgnoterm {\lGammapr,\var \lxone \ltyone} \leone \lty \\
     \ljudgnoterm {\lGammapr,\var \lxtwo \ltytwo} \letwo \lty
     \end{array}}
    {\ljudgnoterm
      {\lGamma, \lGammapr}
      {\lcasee \le \lxone \leone \lxtwo \letwo}
      \lty}

    \inferrule
    {\ljudgnoterm {\lbangty \lGamma} \le \lty}
    {\ljudgnoterm
      {\lbangty \lGamma}
      {\lsrcsharee \le}
      {\lbangty \lty}}

    \inferrule
    {\ljudgnoterm \lGamma \le {\lbangty \lty}}
    {\ljudgnoterm \lGamma {\lcopye {\const\lty} \le} \lty}
\end{mathpar}

In these rules, contexts $\lGamma$ are lists of hypotheses, not sets,
and the comma operation $\lGamma, \lGammapr$ means list concatenation;
the number of times a given hypothesis occurs in $\lGamma$ matters as
each occurrence must be used exactly once. The ``exchange'' rule says
that the order of hypotheses does not matter.

When doing proof search or writing a program (we have the type of the
program we want and the context, but are looking for what to write),
inference rules should be read bottom up: if you need to prove the
lower part (write a program whose type is described by the
lower part), then using this rule reduces it to proving the upper
part. The ``contraction'' and ``weakening'' rules, read bottom-up,
say that an hypothesis can be duplicated or discarded, but only if it
is of the form $\lbangty \lty$ -- others must be consumed exactly
once.

In intuitionistic logic (common typed lambda-calculi), the ``axiom''
rule usually has a conclusion of the form $\lGamma, \lty \lder \lty$.
The goal $\lty$ can be proven if it occurs as \emph{one of} of the
hypotheses in the context. This formulation allows to discard some
hypotheses without using them -- all those in $\lGamma$, so it is not
used in linear logic. Instead, this rule has a conclusion of the form
$\lty \lder \lty$, that only mentions the variable consumed by the
rule. Similarly, the rules with several sub-derivations precisely
account for the resource usage of each subterm: if the results of two
sub-derivations must both be consumed to produce the proof, they
cannot use the same context as it would its hypotheses more than
once. In the pair-forming rule, for example,
\begin{mathpar}
\newcommand{\ljudgnoterm}[3]{#1 \lder #3}
\renewcommand{\var}[2]{#2}
    \inferrule
    {\ljudgnoterm \lGammaone \leone \ltyone \+
     \ljudgnoterm \lGammatwo \letwo \ltytwo}
    {\ljudgnoterm
      {\lGammaone, \lGammatwo}
      {\lpaire \leone \letwo}
      {\lopairty \ltyone \ltytwo}}  
\end{mathpar}
the hypotheses used to prove $\ltyone$ are concatenated with the
hypotheses used to prove $\ltytwo$, preserving the invariant that each
hypothesis in the context is used exactly once in the proof. For
example, $\ltyone, \ltytwo \lder \lopairty \ltytwo \ltyone$ is
provable (remember that the order of hypotheses in the context does
not matter, thanks to the exchange rule), but
$\ltyone \lder \lopairty \ltyone \ltyone$ or
$\ltyone, \ltytwo, \ltythree \lder \lopairty \ltytwo \ltyone$ are
not.

Notice that the rule to eliminate linear sums
$\losumty \ltyone \ltytwo$ does not quite work that way: the same
context $\lGammapr$ is used to type-check two sub-derivations,
$\lGammapr, \ltyone \lder \lty$ and
$\lGammapr, \ltytwo \lder \lty$. Indeed, a proof of
$\losumty \ltyone \ltytwo$ contains either a proof of $\ltyone$ or
a proof of $\ltytwo$, but not both, so only one of those two
sub-derivations will be ``consumed'' to produce the goal $\lty$; they
share the same hypotheses, so that those hypotheses are consumed
(once) in any case.

We will not discuss in this document the two rules to introduce or
eliminate the type $\lbangty$ -- they will be discussed in the talk.

\section{A direct term language}

It is easy to write a ``term language'' (a core programming
language syntax) for most of the logic rules above, taking inspiration
from standard $\lambda$-calculus or program syntax.
\begin{mathpar}
    \inferrule
    { }
    {\ljudgnoconf {\var \lx \lty} \lx \lty}
    \\
    \inferrule
    {\ljudgnoconf \lGammaone \leone \ltyone \+
     \ljudgnoconf \lGammatwo \letwo \ltytwo}
    {\ljudgnoconf
      {\lGammaone, \lGammatwo}
      {\lpaire \leone \letwo}
      {\lopairty \ltyone \ltytwo}}

    \inferrule
    {\begin{array}{c}
       \ljudgnoconf \lGamma \le {\lopairty \ltyone \ltytwo} \\
       \ljudgnoconf {\lGammapr, \var \lxone \ltyone, \var \lxtwo \ltytwo}
         \lepr \lty
     \end{array}}
    {\ljudgnoconf
      {\lGamma, \lGammapr}
      {\lletpaire \lxone \lxtwo \le \lepr}
      \lty}
\\
    \inferrule
    { }
    {\ljudgnoconf {} \lunite \lunitty}

    \inferrule
    {\ljudgnoconf \lGamma \le \lunitty \+
     \ljudgnoconf \lGammapr \lepr \lty}
    {\ljudgnoconf
      {\lGamma, \lGammapr}
      {\lletunite \le \lepr}
      \lty}
    \\
    \inferrule
    {\ljudgnoconf {\lGamma, \var \lx \lty} \le \ltypr}
    {\ljudgnoconf \lGamma {\lfune \lx \lty \le} {\lofunty \lty \ltypr}}

    \inferrule
    {\ljudgnoconf \lGamma \le {\lofunty{\ltypr}{\lty}} \+
     \ljudgnoconf \lGammapr \lepr \ltypr}
    {\ljudgnoconf
      {\lGamma, \lGammapr}
      {\lappe \le \lepr}
      \lty}
    \\
    \inferrule
    {\ljudgnoconf \lGamma \le \ltyi}
    {\ljudgnoconf \lGamma
      {\lsume \ix \le} {\losumty \ltyone \ltytwo}}

    \inferrule
    {\begin{array}{c}\\
       \ljudgnoconf \lGamma \le {\losumty \ltyone \ltytwo}
     \end{array} 
     \+
     \begin{array}{c}
     \ljudgnoconf {\lGammapr,\var \lxone \ltyone} \leone \lty \\
     \ljudgnoconf {\lGammapr,\var \lxtwo \ltytwo} \letwo \lty
     \end{array}}
    {\ljudgnoconf
      {\lGamma, \lGammapr}
      {\lcasee \le \lxone \leone \lxtwo \letwo}
      \lty}
\end{mathpar}

The rules for exchange, contraction and weakening, however, are rather
awkward. For exchange, the easiest thing to do is to \emph{not write
  anything}:
\begin{mathpar}
  \inferrule[exchange]
  {\lGammaone, \var \lxtwo \ltytwo, \var \lxone \ltyone, \lGammatwo
   \lder \le : \ltypr}
  {\lGammaone, \var \lxone \ltyone, \var \lxtwo \ltytwo, \lGammatwo
   \lder \le : \ltypr}
\end{mathpar}

Reasonable syntaxes for contraction and weakening are as follows:
\newcommand{\lletdup}[4]{%
  \lfont{let}\,\lfontsym{(}{#1},{#2}\lfontsym{)}\,%
  \lfontsym{=}\,\lfont{dup}\,#3\,%
  \lfont{in}\,#4}
\newcommand{\ldrop}[2]{\lfont{drop}\,#1\,\lfont{in}\,#2}
\begin{mathpar}
  \inferrule
  {\lGamma, \var \lxone {\lbangty \lty}, \var \lxtwo {\lbangty \lty}
    \lder \le : \ltypr}
  {\lGamma, \var \lx {\lbangty \lty}
    \lder {\lletdup \lxone \lxtwo \lx \le} : \ltypr}
  
  \inferrule
  {\lGamma \lder \le : \ltypr}
  {\lGamma, \var \lx {\lbangty \lty} \lder {\ldrop \lx \le} : \ltypr}
\end{mathpar}

Contraction duplicates a variable $\lx$ (of duplicable type
$\lbangty \lty$) into two different variables/hypotheses, and
weakening drops a variable from the context. But using those to write
programs in practice is a pain. For example, to write a function of
type
$\lofunty {\lbangty \lty} {(\lopairty {\lbangty \lty} {\lbangty \lty})}$,
I would like to be able to write just
$\lfune \lx {\lbangty \lty} {\lpaire \lx \lx}$, instead of having to
do variable plumbing and write
\begin{mathpar}
  \lfune \lx {\lbangty \lty}
  {\lletdup \lxone \lxtwo \lx {\lpaire \lxone \lxtwo}}
\end{mathpar}
and the syntax that follows allows just that.

\section{Nicer syntax for (sub)structural rules}

Instead of a comma operation $\lGamma, \lGammapr$ defined as
concatenation of two context lists, we define a context-joining
operation $\ctxjoin \lGamma \lGammapr$ with the following properties:
\begin{itemize}
\item a variable of duplicable type $\var \lx {\lbangty \lty}$ that
  occurs in $\ctxjoin \lGamma \lGammapr$ may occur in \emph{both}
  $\lGamma$ and $\lGammapr$.
\item other variables must appear on exactly one of the two sides.
\end{itemize}

The precise definition is as follows:
\begin{mathpar}
    \begin{array}{lll@{\qquad\qquad}l}
      \ctxjoin {(\lGammaone, \var \lx {\lbangty \lty})}
               {(\lGammatwo, \var \lx {\lbangty \lty})}
      & \defeq
      & (\ctxjoin \lGammaone \lGammatwo), \var \lx {\lbangty \lty}
      &
      \\ \ctxjoin {(\lGammaone, \var \lx \lty)} \lGammatwo
      & \defeq
      & (\ctxjoin \lGammaone \lGammatwo), \var \lx \lty
      & (\lx \notin \lGammatwo)
      \\ \ctxjoin \lGammaone {(\lGammatwo, \var \lx \lty)}
      & \defeq
      & (\ctxjoin \lGammaone \lGammatwo), \var \lx \lty
      & (\lx \notin \lGammaone)
    \end{array}
\end{mathpar}

Rules can now use this operator instead of context concatenation:
\begin{mathpar}
  \inferrule
    {\ljudgnoconf \lGammaone \leone \ltyone \+
     \ljudgnoconf \lGammatwo \letwo \ltytwo}
    {\ljudgnoconf
      {\ctxjoin \lGammaone \lGammatwo}
      {\lpaire \leone \letwo}
      {\lopairty \ltyone \ltytwo}}
\end{mathpar}

If we read the rule from bottom to top, a variable of type
$\lbangty \lty$ appearing in the context
$\ctxjoin \lGammaone \lGammatwo$ can flow into both $\lGammaone$ and
$\lGammatwo$ -- just like variables in common programming languages,
that can occur in any subterm in scope. With this rule, we can write
as we expected
\begin{mathpar}
  \infer
  {\infer{
     \var \lx {\lbangty \lty} \lder \lx : \lty
     \\
     \var \lx {\lbangty \lty} \lder \lx : \lty
    }{
      \var \lx {\lbangty \lty}
      \lder \lpaire \lx \lx
      : \lopairty {\lbangty \lty} {\lbangty \lty}}
  }
  {\lder \lfune \lx {\lbangty \lty} {\lpaire \lx \lx} 
   : \lofunty {\lbangty \lty} {(\lopairty {\lbangty \lty} {\lbangty \lty})}}
\end{mathpar}
(the topmost inference rule is valid because
$\ctxjoin {(\var \lx {\lbangty \lty})} {(\var \lx {\lbangty \lty})} = (\var \lx {\lbangty \lty})$)

This presentation allows to implicitly duplicate variables of
duplicable type $\lbangty \lty$, but not to implicitly drop them. This
is done by changing the ``leaf rules'', those that do not have any
premise, to allow to forget an arbitrary context $\lbangty \lGamma$,
that is a context where all types are duplicable.
\begin{mathpar}
    \inferrule
    { }
    {\ljudgnoconf {\lbangty \lGamma, \var \lx \lty} \lx \lty}

    \inferrule
    { }
    {\ljudgnoconf {\lbangty \lGamma} \lunite \lunitty}  
\end{mathpar}

That's it. Now, for any hypothesis that you wish to ignore silently,
you can carry it around until it arrives at a leave of the derivation
tree, and stuff it in this context dustbin $\lbangty \lGamma$ there --
assuming it is of droppable type $\lbangty \lty$, of course.

To summarize, the complex set of rules is as follows:
\begin{mathpar}
    \inferrule
    { }
    {\ljudgnoconf {\lbangty \lGamma, \var \lx \lty} \lx \lty}
\\
    \inferrule
    {\ljudgnoconf \lGammaone \leone \ltyone \+
     \ljudgnoconf \lGammatwo \letwo \ltytwo}
    {\ljudgnoconf
      {\ctxjoin \lGammaone \lGammatwo}
      {\lpaire \leone \letwo}
      {\lopairty \ltyone \ltytwo}}

    \inferrule
    {\begin{array}{c}
       \ljudgnoconf \lGamma \le {\lopairty \ltyone \ltytwo} \\
       \ljudgnoconf {\lGammapr, \var \lxone \ltyone, \var \lxtwo \ltytwo}
         \lepr \lty
     \end{array}}
    {\ljudgnoconf
      {\ctxjoin \lGamma \lGammapr}
      {\lletpaire \lxone \lxtwo \le \lepr}
      \lty}
\\
    \inferrule
    { }
    {\ljudgnoconf {\lbangty \lGamma} \lunite \lunitty}

    \inferrule
    {\ljudgnoconf \lGamma \le \lunitty \+
     \ljudgnoconf \lGammapr \lepr \lty}
    {\ljudgnoconf
      {\ctxjoin \lGamma \lGammapr}
      {\lletunite \le \lepr}
      \lty}
\\
    \inferrule
    {\ljudgnoconf {\lGamma, \var \lx \lty} \le \ltypr}
    {\ljudgnoconf \lGamma {\lfune \lx \lty \le} {\lofunty \lty \ltypr}}

    \inferrule
    {\ljudgnoconf \lGamma \le {\lofunty{\ltypr}{\lty}} \+
     \ljudgnoconf \lGammapr \lepr \ltypr}
    {\ljudgnoconf
      {\ctxjoin \lGamma \lGammapr}
      {\lappe \le \lepr}
      \lty}
\\
    \inferrule
    {\ljudgnoconf \lGamma \le \ltyi}
    {\ljudgnoconf \lGamma
      {\lsume \ix \le} {\losumty \ltyone \ltytwo}}

    \inferrule
    {\begin{array}{c}\\
       \ljudgnoconf \lGamma \le {\losumty \ltyone \ltytwo}
     \end{array} 
     \+
     \begin{array}{c}
     \ljudgnoconf {\lGammapr,\lxone:\ltyone} \leone \lty \\
     \ljudgnoconf {\lGammapr,\lxtwo:\ltytwo} \letwo \lty
     \end{array}}
    {\ljudgnoconf
      {\ctxjoin \lGamma \lGammapr}
      {\lcasee \le \lxone \leone \lxtwo \letwo}
      \lty}
\\
    \inferrule
    {\ljudgnoconf {\lbangty \lGamma} \le \lty}
    {\ljudgnoconf
      {\lbangty \lGamma}
      {\lsrcsharee \le}
      {\lbangty \lty}}

    \inferrule
    {\ljudgnoconf \lGamma \le {\lbangty \lty}}
    {\ljudgnoconf \lGamma {\lcopye {\const\lty} \le} \lty}
\end{mathpar}

\paragraph{Remark:} whether or not this syntactic change is a good
idea depends on the operational semantics of contraction and weakening
in your programming language. If those are no-ops, then having them
invisible in the syntax makes the most sense. If, on the contrary,
they have a runtime meaning (they actually copy values or drop
values around), as they have in the Rust language for example, then
the programmer may want to be more careful about where exactly they
occur, and thus have them present in the syntax.
\end{document}