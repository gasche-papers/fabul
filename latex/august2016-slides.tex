\documentclass[svgnames,notheorems]{beamer}

%\usetheme{Madrid} % My favorite!
\usetheme{Boadilla} % Pretty neat, soft color.
%\usetheme{default}
%\usetheme{Warsaw}
%\usetheme{Bergen} % This template has nagivation on the left
% \usetheme{Frankfurt} % Similar to the default
%with an extra region at the top.
%\usecolortheme{seahorse} % Simple and clean template
%\usetheme{Darmstadt} % not so good
% Uncomment the following line if you want %
% page numbers and using Warsaw theme%
% \setbeamertemplate{footline}[page number]
%\setbeamercovered{transparent}
\setbeamercovered{invisible}
% To remove the navigation symbols from
% the bottom of slides%
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{\hfill\insertframenumber\hfill\vspace{3mm}}

% to avoid confusion with \ob{} color (blue), use orange-like titles
% \setbeamercolor*{titlelike}{fg=Orange!80!Purple}

\let\Title\title
% beamer defined \title, we want to keep it as \Title as mttex will redefine it

\usepackage[omit,todo]{mttex}
\usepackage{lib}
\usepackage{beamer_lib}

\usepackage[all]{xy}

\usepackage{listings}

\lstset{xleftmargin=2em}
\lstset{
  mathescape=true,
  language=[Objective]{Caml},
  basicstyle=\sffamily,
  extendedchars=true,
  showstringspaces=false,
  aboveskip=\smallskipamount,
  % belowskip=\smallskipamount,
  columns=flexible,
  deletekeywords={true,false},
  otherkeywords={where,pattern},
  keepspaces=true,
}

\Title{Full abstraction for multi-language systems\\ ML plus linear types}
\author{\textbf{Gabriel Scherer}, Amal Ahmed, Max New}

\institute{Northeastern University}
\date{\today}

% \usepackage{tcolorbox}

% \tcbset{boxsep=-1mm,boxrule=0.3mm,colback=white}
% \newtcbox{\noninvertiblerule}{colframe=red!75!black}
% \newtcbox{\invertiblerule}{colframe=green!75!black}

\begin{document}
%
\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Multi-language systems}
  
  Languages of today tend to evolve into behemoths by piling features
  up: C++, Scala, GHC Haskell, OCaml...

  \vfill

  Multi-language systems: several languages working together to cover
  the feature space. {\color{gray}{(simpler?)}}

  \vfill

  Multi-language system \emph{design} may include designing new
  languages for interoperation.

  \vfill

  Full abstraction to understand graceful language interoperability.
\end{frame}

\begin{frame}
  \frametitle{Full abstraction for multi-language systems}

  $\sembrace \wild : S \to T$ fully abstract:
  \begin{mathpar}
    a \mathrel{\ctxeqvsym} b
    \implies
    \sembrace a \mathrel{\ctxeqvsym} \sembrace b
  \end{mathpar}

  \vfill

  Full abstraction preserves (equational) reasoning.

  \vfill\pause

\begin{displaymath}
    \xymatrix{
        S_1 \ar[dr]_{\text{full abs.}} 
        \only<-2>{{\ar@{{}{}{}}[r]^{\text{\color{white}{interop}}}}}
        \only<3->{\ar@{{}~{}}[rr]^{\text{interop.}}} & & S_2 \ar[dl]^{\text{full abs.}} \\
        & T &
      }
\end{displaymath}

  Mixed $S_1, S_2$ programs preserve (equational) reasoning of their fragments.
  \pause
  Graceful multi-language semantics.

  {\color{gray}{(or vice versa)}}
\end{frame}

\begin{frame}
  \frametitle{Which languages?}

  ML sweet spot hard to beat,
  
  but ML programmers yearn for language extensions.

  \vfill

  ML plus:
  \begin{itemize}
  \item low-level memory, resource tracking, ownership
  \item effect system
  \item theorem proving
  \item \dots
  \end{itemize}

  \vfill

  In this talk: a first ongoing experiment on \emph{ML} plus \emph{linear types}.
\end{frame}

\newcommand{\termpart}[1]{\Alt<2->{#1}{}}
\newcommand{\simplejudg}[3]{#1 \vdash \termpart{#2 :} #3}
\newcommand{\ljudgsimple}[5]{\simplejudg{#2}{#4}{#5}}
\newcommand{\ljudgsimplenoconf}[3]{\simplejudg{#1}{#2}{#3}}

\begin{frame}
  \frametitle{Linear types: base}

  A \emph{simple} but \emph{useful} language with linear types.

  \begin{mathpar}
    \inferrule
    {\ljudgsimple \lstoretyone \lGammaone \lstoreone \leone \ltyone \+
     \ljudgsimple \lstoretytwo \lGammatwo \lstoretwo \letwo \ltytwo}
    {\ljudgsimple {\storectxjoin \lstoretyone \lstoretytwo}
            {\ctxjoin \lGammaone \lGammatwo}
            {\lstorejoin \lstoreone \lstoretwo}
            {\lpaire \leone \letwo}
            {\lopairty \ltyone \ltytwo}}

    \inferrule
    {\ljudgsimple \lstoretya \lGamma \lstore \le {\lopairty \ltyone \ltytwo} \+
     \ljudgsimple \lstoretyb
        {\lDelta,
          \termpart{\lxone :} \ltyone,
          \termpart{\lxtwo :} \ltytwo}
            \lstorepr \lepr \lty}
    {\ljudgsimple {\storectxjoin \lstoretya \lstoretyb}
            {\ctxjoin \lGamma \lDelta}
            {\lstorejoin \lstore \lstorepr}
            {\lletpaire \lxone \lxtwo \le \lepr}
            \lty}
\\
    \inferrule
    { }
    {\ljudgsimplenoconf {\lbangty \lGamma} \lunite \lunitty}

    \inferrule
    {\ljudgsimple \lstoretya \lGamma \lstore \le \lunitty \+
     \ljudgsimple \lstoretyb \lDelta \lstorepr \lepr \lty}
    {\ljudgsimple
      {\storectxjoin \lstoretya \lstoretyb}
      {\ctxjoin \lGamma \lDelta}
      {\lstorejoin \lstore \lstorepr}
      {\lletunite \le \lepr}
      \lty}
\\
    \inferrule
    {\ljudgsimple  \lstorety {\lGamma, \termpart{\lx :} \lty}  \lstore \le \ltypr}
    {\ljudgsimple \lstorety \lGamma
      \lstore {\lfune \lx \lty \le}
      {\lofunty \lty \ltypr}}

    \inferrule
    {\ljudgsimple \lstoretya \lGamma \lstore \le {\lofunty{\ltypr}{\lty}} \+
     \ljudgsimple \lstoretyb \lDelta \lstorepr \lepr \ltypr}
    {\ljudgsimple
      {\storectxjoin \lstoretya \lstoretyb}
      {\ctxjoin \lGamma \lDelta}
      {\lstorejoin \lstore \lstorepr}
      {\lappe \le \lepr}
      \lty}
\\
%     \inferrule
%     {\ljudgsimple \lstorety \lGamma \lstore \le \ltyi}
%     {\ljudgsimple \lstorety \lGamma \lstore
%       {\lsume \ix \le} {\losumty \ltyone \ltytwo}}
    
%     \inferrule
%     {\ljudgsimple \lstoretya \lGamma \lstore \le {\losumty \ltyone \ltytwo}  \+
%      \ljudgsimple \lstoretyb {\lDelta,\lxone:\ltyone} \lstorepr \leone \lty \+
%      \ljudgsimple \lstoretyb {\lDelta,\lxtwo:\ltytwo} \lstorepr \letwo \lty}
%     {\ljudgsimple
%       {\storectxjoin \lstoretya \lstoretyb}
%       {\ctxjoin \lGamma \lDelta}
%       {\lstorejoin \lstore \lstorepr}
%       {\lcasee \le \lxone \leone \lxtwo \letwo}
%       \lty}
% \\
%     \bidirliniso
%       {\lmuty \lalpha \lty}
%       {\lunfoldedty \lalpha \lty}
%       {\lunfolde {}}
%       {\lfolde {\lmuty \lalpha \lty} {}}
% \\
    \inferrule
    {\ljudgsimple \lstorety {\lbangty \lGamma} \lstore \le \lty}
    {\ljudgsimple \lemptystorety
      {\lbangty \lGamma}
      \lemptystore
      {\Alt<3->{\lopsharee {\const \lty} \le}{}}
      {\lbangty \lty}}

    \inferrule
    {\ljudgsimple \lstorety \lGamma \lstore \le {\lbangty \lty}}
    {\ljudgsimple \lstorety \lGamma \lstore {\Alt<4->{\lcopye {\const\lty} \le}{}} \lty}
% \\
%     \inferrule
%     { }
%     {\ljudgsimple
%       {\ladead \lloc \lty}
%       {\lbangty \wild}
%       {\stsingleton \lloc \lempty}
%       \lloc
%       {\lboxty \lzero \lty}}
%
%     \inferrule
%     {\ljudgsimple \lstorety \lGamma \lstore \lv \lty}
%     {\ljudgsimple
%       {(\lstoreassert \lGamma \lstorety \lloc \lboxty \lone \lty)}
%       {\ctxjoin {\lbangty \wild} \lGamma}
%       {\stsingletonconf \lloc \lstore \lv} \lloc {\lboxty \lone \lty}}
% \\
% \bidirliniso
%   {\lunitty}
%   {\lboxty \lzero \lty}
%   {\lnewe {}}
%   {\lfreee {}}
%
% \bidirliniso
%   {\lboxedty \lty}
%   {\lunboxedty \lty}
%   {\lunboxe {}}
%   {\lboxe {}}
% \\
%     \bidirliniso
%       {\lbangty {(\lopairty \ltyone \ltytwo)}}
%       {\lopairty {\lbangty \ltyone} {\lbangty \ltytwo}}
%       {\lcopypaire {}}
%       {\lopsharepaire {}}
%
%     \bidirliniso
%       {\lbangty \lunitty}
%       {\lunitty}
%       {\lcopybange {}}
%       {\lopsharebange {}}
%
%     \bidirliniso
%       {\lbangty {(\lofunty \lty \ltypr)}}
%       {\lofunty \lty \ltypr}
%       {\lcopyfune {}}
%       {\lopsharefune {}}
%
%     \bidirliniso
%       {\lbangty {(\losumty \ltyone \ltytwo)}}
%       {\losumty {\lbangty \ltyone} {\lbangty \ltytwo}}
%       {\lcopysume {}}
%       {\lopsharesume {}}
%
%     \bidirliniso
%       {\lbangty {\lbangty \lty}}
%       {\lbangty \lty}
%       {\lcopybange {}}
%       {\lopsharebange {}}
%
%    \bidirliniso
%       {\lbangty {\lmuty \lalpha \lty}}
%       {\lbangty {\lunfoldedty \lalpha \lty}}
%       {\lcopymue \lalpha \lty {}}
%       {\lopsharemue \lalpha \lty {}}
%
%     \bidirliniso
%       {\lbangty {\lboxty \lzero \lty}}
%       {\lboxty \lzero \lty}
%       {\lcopyboxe \lzero {}}
%       {\lopshareboxe \lzero {}}
%
%     \bidirliniso
%       {\lbangty {\lboxty \lone \lty}}
%       {\lopairty {\lbangty {\lboxty \lzero \lty}} {\lbangty \lty}}
%       {\lcopyboxe \lone {}}
%       {\lopshareboxe \lone {}}
  \end{mathpar}
\end{frame}

\begin{frame}
  \frametitle{Applications}

  Protocol with resource handling requirements.
  
  \vspace{1em}

  ``This file descriptor must be closed''

  \vfill

  Typestate.
\end{frame}

\begin{frame}
  \frametitle{Linear types: linear locations}

  $\lboxty \lone \lty$: full cell

  \vspace{1em}

  $\lboxty \lzero \lty$: empty cell

  \vfill

  \begin{mathpar}
  \bidirliniso
    {\lunitty}
    {\lboxty \lzero \lty}
    {\lnewe {}}
    {\lfreee {}}
  
  \bidirliniso
    {\lboxedty \lty}
    {\lunboxedty \lty}
    {\lunboxe {}}
    {\lboxe {}}    
  \end{mathpar}
\end{frame}

\begin{frame}
  \frametitle{Applications}

  In-place reuse of memory cells.
\end{frame}

\begin{frame}[fragile]
  \frametitle{List reversal}

\begin{lstlisting}
  type LList a = $\mu$t. 1 $\oplus$ Box 1 (a $\otimes$ t)
  pattern Nil = inl ()
  pattern Cons l x xs = inr (box (l, (x, xs)))
  
  val reverse : LList a $\multimap$ LList a
  let reverse list = loop Nil list
    where rec loop tail = function
    | Nil $\to$ tail
    | Cons l x xs $\to$ loop (Cons l x tail) xs

  (* use reverse internally *)

  (* on the ML side *)
  type List a = $\mu$t. 1 + (a $\times$ t)
  let reverse list = UL(share (reverse (copy (LU(list)))))
\end{lstlisting}
\end{frame}

\renewcommand{\defeq}{\stackrel{\text{def}}{=}}

\begin{frame}
  \frametitle{Full abstraction}

  The ML language can be compiled into a linear language.\pause
  
\begin{mathpar}
  \tyLU \uty
  \quad\defeq\quad
  \lbangty {\erase \uty}

  \begin{array}{lll}
    \\ \erase{\upairty \utyone \utytwo}
    & \defeq
    & \lboxty \lone {(\lopairty {\erase \utyone} {\erase \utytwo})}
    \\ \erase{\uunitty}
    & \defeq
    & \lunitty
    \\ \erase{\ufunty \utyone \utytwo}
    & \defeq
    & \lofunty {\tyLU \utyone} {\tyLU \utytwo}
  \end{array}
\end{mathpar}

  This gives a direct multi-language semantics.

  \vfill\pause

  Full abstraction by pure interpretation of the linear language in ML.\pause

\begin{mathpar}
  \begin{array}{lll}
    \lift {\lbangty \lty}
    & \defeq
    & \lift \lty
    \\ \lift {\lboxty \lone \lty}
    & \defeq
    & {\lift \lty}
    \\ \lift {\lboxty \lzero \lty}
    & \defeq
    & \uunitty
    \\ \lift {\lopairty \ltyone \ltytwo}
    & \defeq
    & \upairty {\lift \ltyone} {\lift \ltytwo}
  \end{array}
\end{mathpar}

{\color{gray}{(Cogent)}}
\end{frame}

\begin{frame}
  \frametitle{Going further}

  Polymorphism not formalized yet.

  \vfill

  Implementation?
\end{frame}
\end{document}

%% Notes after the talk
%
% People did not immediately grasp the fine-grained granularity of
% language boundaries. Develop the example, for example, of
%
%   ∀{L}(α). τ := LU(∀α.UL(τ))
%   Λ{L}(α).e := LU(Λα.UL(e))


% What is a multi-language semantics?
%
%
%   Suggestion of Max:
% 
%      multi(S₁,S₂) is
%
%        T
%        C₁ : S₁ → T   equiv.-reflecting
%        C₁ : S₂ → T   equiv.-reflecting
%
%     The language is (S₁ + S₂), its semantics is given by
%       programs: Img(C₁) ∪ Img(C₂)
%       values: values of T in the image
%       equivalence: equivalence of T on the image
%   .
%
%   Wait, is the equivalence on S₁+S₂ the restriction of the
%   equivalence of T on its image, or the contextual equivalence
%   induced by the contexts in S₁+S₂ (as opposed to all contexts in
%   T)?
%
%   If C₁,C₂ fully-abstract, or if T is "tight" (the usual syntactic
%   multi-language construction), then both coincide – in the latter
%   case, T is exactly (S₁+S₂) so the contexts are the same.


% Consider
%
%
%  Γ ⊢ e₁ : !τ'
%  Γ, x:τ' ⊢ e₂ : τ
%  Γ ⊢ map x = e₁ in e₂ : !τ

% join : m m a -> m a
% bind : (a -> m b) -> m a -> m b
% bind f = join ∘ map{m} f

% cojoin : m a -> m m a
% cobind : (m a -> b) -> m a -> m b
% cobind f a = map {m} f a ∘ cojoin

% map : (a -> b) -> m a -> m b

% map x = e₁ in e₂
% let x = copy e₁ in share e₂

% !a ⊗ !b ⊸ !(a ⊗ b)
% share⊗ (x, y) =
%   with x y share (x, y)

% share⊕ z =
%   case z of
%   | L x -> with x share (inj x)
%   | R y -> with x share (inj y)

% share⊗ (x, y) = share (copy x, copy y)


% !a ⊗ !b ⊸ !(a ⊗ b)
% share⊗ (x, y) = share{a ⊗ b} (copy x, copy y)



% share⊗ (LU(x), LU(y)) = UL(x,y)


% ⌊_⌋ : U → L
% ⌈_⌉ : L → U

% copy : LU(σ) ⊸ ⌊σ⌋
% share : ⌊σ⌋ ⊸ LU(σ)


% let reverse li = UL(share (reverse (copy LU(li))))