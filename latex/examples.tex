\section{Hybrid program examples}
\label{sec:examples}

\subsection{In-Place Transformations}

In \fullref{subsec:l:store} we proposed a program for in-place reversal
of linear lists defined by the type
%
$
\lapptyop {LinList} \lty
\defeq
\lmuty \lalpha
  {\losumty
    \lunitty
    {\lboxedty {(\lopairty \lty \lalpha)}}}
$.
%
We can also define a type of ML lists
%
$\uapptyop {List} \uty
\defeq
\umuty \ualpha {\usumty \uunitty {\upairty \uty \ualpha}}
$.
%
Note that ML lists are compatible with shared linear lists,
in the sense that
%
$\uapptyop {List} \uty
\ulcompatbangpar
{\lapptyop {LinList} {\llumpedty \uty}}
$.
%
This enables writing in-place list-manipulation functions in $\llang$,
and exposing them to beginners at a $\ulang$ type:
\begin{smathpar}
  \ufont{rev}~\ufont{xs}
  \defeq
  \UmpLenoconf
    {\lapptyop {LinList} {\llumpedty \uty}}
    {\lsrcsharee
      {(\lfont{rev\_into}
        ~{\lsrccopye {(\LUmpe {\lapptyop {LinList} {\llumpedty \uty}} {\ufont{xs}})}}
        ~\lfont{Nil})}}
\end{smathpar}

This example is arguably silly, as the allocations that are avoided by
doing an in-place traversal are paid when copying the shared list to
obtain a uniquely-owned version. A better example of list operations
that can profitably be sent on the linear side is quicksort, whose
code we give in \fullref{fig:quickort}. An ML implementation allocates
intermediary lists for each recursive call, while the surprisingly
readable $\ulang$ implementation only allocates for the first copy.

\begin{figure}
  \centering
\begin{lstlisting}
/*
partition : !(!$\lalpha$ $\multimap$ Bool) $\multimap$ LList !$\lalpha$ $\multimap$ LList !$\lalpha$ $\oplus$ LList !$\lalpha$
partition p li = partition_aux p (Nil, Nil) li
partition_aux p (yes, no) Nil = (yes, no)
partition_aux p (yes, no) (Cons l x xs) =
  let (yes, no) =
    if copy p x
    then (Cons l x yes, no)
    else (yes, Cons l x no) in
  partition_aux p (yes, no) xs

lin_quicksort : LList !$\lalpha$ $\multimap$ LList !$\lalpha$
lin_quicksort li = quicksort_aux li Nil
quicksort_aux Nil acc = acc
quicksort_aux (Cons l head li) acc =
  let p = share (fun x -> x < head) in
  let (below, above) = partition p li in
  quicksort_aux below (Cons l head (quicksort_aux above acc))*/

/!quicksort li UL(/*li*/) = UL(/*share (lin_quicksort (copy li))*/)!/
\end{lstlisting}
  \caption{Quicksort}
  \label{fig:quickort}
\end{figure}

\subsection{Typestate Protocols}

Linear types can enforce proper allocation and deallocation of
resources, and in general any automata/typestate-like protocols on
their usage by encoding the state transitions as linear
transformations. In the simple example of file-descriptor handling in
the introduction, additional safety compared to ML programming can be
obtained by exposing file-handling functions on the $\ulang$ side,
with linear types. We assumed the following API for linear file handling, which enforces a correct usage protocol:
\newcommand{\lPath}{\llumpedty{\ufont{Path}}}
\newcommand{\lString}{\llumpedty{\ufont{String}}}
\newcommand{\lHandle}{\lfont{Handle}}
\newcommand{\lEmptyHandle}{\lfont{EmptyHandle}}
\begin{smathpar}
  \begin{array}{lll}
    \lfont{open} & :
    & \lbangty{(\lofunty \lPath \lHandle)}
    \\
    \lfont{line} & :
    & \lbangty
        {(\lofunty \lHandle
          {(\losumty \lHandle {(\lopairty \lString \lHandle)})})}
    \\
    \lfont{close} & :
    & \lbangty{(\lofunty \lHandle \lunitty)}
  \end{array}
\end{smathpar}

Another interesting example of protocol usage for which linear types help
is the use of \emph{transient} versions of persistent data structures,
as popularized by Clojure. An unrestricted type
$\uapptyop {Set} \ualpha$ may represent persistent sets as balanced
trees with logarithmic operations performing
path-copying. A $\lfont{transient}$ call returns a mutable version of
the structure that supports efficient batch in-place updates, before
a $\lfont{persistent}$ call freezes this transient structure back into
a persistent tree. To preserve a purely functional semantics, we must
enforce that the intermediate transient value is uniquely owned. We
can do this by using the linear types for the transient API:

\begin{small}
\begin{lstlisting}
/!type Set $\ualpha$
val add : Set $\ualpha$ $\ufunty{}{}$ $\ualpha$ $\ufunty{}{}$ Set $\ualpha$ (* path copy *)
...!/
/*type MutSet $\lalpha$
val add: !(MutSet $\lalpha$ $\lofunty{}{}$ $\lalpha$ $\lofunty{}{}$ MutSet $\lalpha$) (* in-place update *)
...
val transient : !(![/!Set $\ualpha$!/] $\lofunty{}{}$ MutSet ![/!$\ualpha$!/])
val persistent : !(MutSet ![/!$\ualpha$!/] $\lofunty{}{}$ ![/!Set $\ualpha$!/])*/
\end{lstlisting}
\end{small}
