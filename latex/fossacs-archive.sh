#!/bin/bash
mkdir fossacs-archive || { echo "error, you need to (rmdir fossacs-archive)"; exit 1; }

cp article-fossacs-noappendices.{tex,cfg} fossacs-archive/
cp examples.tex fossacs-archive/

for f in article.bib amal.bib
do
    cp $f fossacs-archive/
done

for f in boolean copytofile fixtheorems lib lrlib \
         mathpartir mttex mttex_linear reversion unlist
do
    cp $f.sty fossacs-archive/
done

cd fossacs-archive && zip -r fossacs-archive * && cd ..
mv fossacs-archive/fossacs-archive.zip fossacs-archive.zip
